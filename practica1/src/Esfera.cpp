// Esfera.cpp: implementation of the Esfera class.
//
//////////////////////////////////////////////////////////////////////

#include "Esfera.h"
#include "glut.h"
//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

Esfera::Esfera()
{
	radio=0.5f;
	velocidad.x=3;
	velocidad.y=3;
	pulso=0.02;
}

Esfera::~Esfera()
{
}



void Esfera::Dibuja()
{
	glColor3ub(0,255,0);
	glEnable(GL_LIGHTING);
	glPushMatrix();
	glTranslatef(centro.x,centro.y,0);
	glutSolidSphere(radio,15,15);
	glPopMatrix();
}

void Esfera::Mueve(float t)
{
  	centro.x+= velocidad.x*t;
	centro.y+= velocidad.y*t;
}

void Esfera::Pulso(float t)
{
	//codigo de pulso esfera
	if (radio>=0.05)
		radio-=pulso*t;
}

